
def es_par(numero)->bool:
    respuesta = False
    if numero%2 == 0:
        respuesta = True
    return respuesta

numeros = [1,2,3,4,5,6,7,8,9,10]
respuesta = list(filter(es_par, numeros))
print(respuesta)


'''
1) Filtre la lista 'nombres' por los nombres que comiencen por la letra 'j'
2) Filtre la lista 'nombres' por los nombres que comiencen por la letra 'l'
3) Filtre la lista 'nombres' por los nombres que comiencen por 'l' y retorne 
    una lista con los nombres en mayúscula
'''
nombres = ['Alejandro', 'Liliana', 'Johan', 'juan', 'Laura', 'Jair', 'luis', 'Sebastián']

resp = list(filter(lambda cadena: cadena[0].lower()=='j', nombres))
print('Filter con lambda -> ', resp)
resp = list(filter(lambda cadena: cadena[0].lower()=='l', nombres))

#Solución de Manuel Santiago
def palabra_l(nombre)->bool:
    respuesta=False
    if nombre[0].lower()=='l':
        respuesta=True
    return respuesta
    
def palabra_j(nombre)->bool:
    respuesta=False
    if nombre[0].lower()=='j':
        respuesta=True
    return respuesta

print(list(filter(palabra_l, nombres)))