from functools import reduce


caracteres = ['A', 'B', 'C', 'D', 'E']

convertir_a_minuscula = lambda c: c.lower()

respuesta = list( map(convertir_a_minuscula, caracteres))
print(respuesta)


respuesta = list( map( lambda c: c.lower(), caracteres ) )
print(respuesta)

def simulacion_map(funcion, lista):
    respuesta = []
    for numero in lista:
        resp_funcion = funcion(numero)
        respuesta.append( resp_funcion )
    return respuesta

cuadrado = lambda n: n*n
dividir_x_2 = lambda n: n/2
lista = [10,20,30,40,50,60]

print(simulacion_map(dividir_x_2, lista))


print('--------UTILIZANDO MAP-------')
respuesta = list(map(lambda n: n**2, lista))
print(respuesta)

print('----------------------------------------------------------')



def reemplazar_caracteres_especiales(caracter: str)->str:
    respuesta = caracter
    caracter = caracter.lower()
    if caracter == 'á':
        respuesta = 'a'
    elif caracter == 'é':
        respuesta = 'e'
    elif caracter == 'í':
        respuesta = 'i'
    elif caracter == 'ó':
        respuesta = 'o'
    elif caracter == 'ú':
        respuesta = 'u'
    elif caracter == 'ñ':
        respuesta = 'n'
    return respuesta

caracteres = ['á', 'é', 'r', 'í']
respuesta = list(map(reemplazar_caracteres_especiales, caracteres))
print(respuesta)

lista_cadenas = ['Sebastián', 'María', 'Peña', 'Peñaranda', 'Función', 'Andrés']



resp = list(map(lambda cadena: list(map(reemplazar_caracteres_especiales, cadena)), lista_cadenas))
print(resp)


resp_3 = list(map(lambda cadena: reduce(lambda acumulador,letra:acumulador+letra,tuple(map(reemplazar_caracteres_especiales,cadena))),lista_cadenas))
print(resp_3)

resp_2 = list(map(lambda palabra_separada: "".join(palabra_separada), list(map(lambda cadena: tuple(map(reemplazar_caracteres_especiales,cadena)),lista_cadenas)) ))

