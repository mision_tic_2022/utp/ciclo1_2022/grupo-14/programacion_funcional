

from functools import reduce


numeros = [1,2,3,4,5,6,7,8,9]


resp = reduce(lambda acumulador,n: acumulador+n, numeros)
print(resp)

lista_caracteres = ['H', 'O', 'L', 'A', ' ', 'M', 'U', 'N', 'D', 'O', ' ', 'M', 'I', 'S', 'I', 'O', 'N', ' ', 'T', 'I', 'C', ' ', '-', ' ', 'U', 'T', 'P']

concatenar = lambda acumulador, elemento: acumulador+elemento
mensaje = reduce(concatenar, lista_caracteres)
print(mensaje)