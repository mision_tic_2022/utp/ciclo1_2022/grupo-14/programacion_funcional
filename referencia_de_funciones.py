
def sumar(n1,n2):
    suma = n1+n2
    return suma

resultado = sumar(10,20)

#print(sumar(10,20))
referencia_sumar = sumar

#print(referencia_sumar(20,30))
otra_referencia = referencia_sumar
#print(otra_referencia(30,40))


def crear_operacion(operador: str):
    operacion = ''
    if operador == '+':
        #Crear función local
        def sumar(n1,n2,):
            return (n1+n2)
        operacion = sumar
    elif operador == '-':
        def restar(n1, n2):
            return (n1-n2)
        operacion = restar
    elif operador == '*':
        def multiplicar(n1,n2):
            return (n1*n2)
        operacion = multiplicar
    elif operador == '/':
        def dividir(n1,n2):
            return(n1/n2)
        operacion = dividir
    else:
        def mensaje(n1,n2):
            return f'La operación no existe para {n1} {operador} {n2}'
        operacion = mensaje
    return operacion

operacion = crear_operacion('/') 
print( operacion(10,10) )
