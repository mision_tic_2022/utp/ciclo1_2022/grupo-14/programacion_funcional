def sumar(n1, n2):
    suma = n1+n2
    return suma

#print(sumar(10,10))

def saludar():
    return "Hola mundo"

saludar_lambda = lambda : 'Hola mundo'

#print(saludar_lambda())

sumar_lambda = lambda n1,n2: n1+n2
#print(sumar_lambda(20,20))


def crear_operacion(operador: str):
    operacion = lambda n1, n2: f'La operación no existe para {n1} {operador} {n2}'
    #Evaluar el operador
    if operador == '+': operacion = lambda n1,n2: n1+n2
    elif operador == '-': operacion = lambda n1,n2: n1-n2
    elif operador == '*': operacion = lambda n1,n2: n1*n2
    elif operador == '/': operacion = lambda n1,n2: n1/n2
    elif operador == '**': operacion = lambda n1,n2: n1**n2 
    
    return operacion

operacion = crear_operacion('*')

#print(operacion(5,2))


conversion = lambda diccionario: {**diccionario}


miDiccionario = {
    'nombre':'Juan',
    'apellido': 'Perez',
    'edad': 25
}

print( conversion(miDiccionario) )


'''
1) Desarrolle una función anónima que reciba como parámetro una lista de números,
    retorne el promedio. (sum(lista))

'''

promedio = lambda lista: sum(lista)/len(lista)
print(promedio([4.5,4.2,3.5]))