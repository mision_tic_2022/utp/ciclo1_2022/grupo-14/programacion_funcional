
nombres = ['Alejandro', 'Luis', 'Andrés']
apellidos = ['Carvajal', 'Torres', 'Perez', 'Quintero']

respuesta = list(zip(nombres, apellidos))
print(respuesta)

print('----------------------')
numeros_1 = [1,2,3,4,5,6,7,8,9]
numeros_2 = [9,8,7,6,5,4,3,2,1]
numeros_3 = [10,20,30,40,50,60,70,80,90]

union_1 = list(zip(numeros_1, numeros_2, numeros_3))
print(union_1)

#union_2 = list( zip(union_1, numeros_3) )
#print(union_2)



print('-------------------------------')
lista_1 = [1,2,3]
lista_2 = [5,6,7]
respuesta = tuple(zip(lista_1, lista_2))
print(respuesta)

