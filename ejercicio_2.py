'''
A -> #
B -> @
C -> -
'''

from functools import reduce


caracteres_especiales = ['#', '@', '-', '.', '$', '(', ')', '=', '?', '¡', '!', '%']
abecedario = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l']

relacion_caracteres = list(zip(abecedario, caracteres_especiales))
print(relacion_caracteres)
print('--------------------')

def codificar(mensaje, relacion_caracteres):
    #Filtrar de la lista de tuplas los caracteres especiales
    filtro = lambda caracter: list(filter(lambda tupla: tupla[0]==caracter, relacion_caracteres))[0][1]
    #Mapear los datos para obtener una lista de caracteres especiales
    codificacion = list(map(filtro, mensaje))
    #Reducir la lista a un String
    codificacion = reduce(lambda ac, el: ac+el, codificacion)
    return codificacion

def decodificar(mensaje_codificado, relacion_caracteres):
    filtro_letras = lambda caracter: list(filter(lambda tupla: tupla[1]==caracter, relacion_caracteres ))[0][0]
    decodificacion = list(map(filtro_letras, mensaje_codificado))
    decodificacion = reduce( lambda ac, el: ac+el, decodificacion )
    return decodificacion

palabra_1 = "ceja"
palabra_2 = "jabali"
cod_1 = codificar(palabra_2, relacion_caracteres)
print(cod_1)
dec_1 = decodificar(cod_1, relacion_caracteres)
print(dec_1)