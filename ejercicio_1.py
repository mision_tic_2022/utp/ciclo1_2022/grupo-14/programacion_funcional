#Solución de Luis Alejandro
def reemplazar_caracteres_especiales_palabra(palabra: str)->str:
    respuesta_palabra=""
    for caracter in palabra:
        caracter = caracter.lower()
        respuesta=caracter
        if caracter == "á":
            respuesta = "a"
        elif caracter == "é":
            respuesta="e"
        elif caracter == "í":
            respuesta="i"
        elif caracter == "ó":
            respuesta="o"
        elif caracter == "ú":
            respuesta="u"
        elif caracter == "ñ":
            respuesta="n"
        respuesta_palabra+=respuesta
        # print(respuesta)
    return respuesta_palabra


lista_cadenas = ['Sebastián', 'María', 'Peña', 'Peñaranda', 'Función', 'Andrés']

respuesta_lista=list(map(reemplazar_caracteres_especiales_palabra,lista_cadenas))
print(respuesta_lista)
