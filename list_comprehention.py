
numeros = [10,15,20,25,30,40,45,50,60,65,70,75,80,85,90,95]

numeros__al_cuadrado = [n**2 for n in numeros ]
print(numeros__al_cuadrado)

numeros_pares_al_cuadrado = [n**2 for n in numeros if n%2==0 ]
print(numeros_pares_al_cuadrado)

numeros_pares_al_cuadrado = [n**2 if n%2==0 else n for n in numeros]
print(numeros_pares_al_cuadrado)


lista_nombres = ['Sebastián', 'María', 'Peña', 'Peñaranda', 'Fernando', 'Andrés']

lista = [n if n[0].upper()=='P' else 'NULL' for n in lista_nombres ]
print(lista)

'''
Utilizando list comprehention:
1) Genere una lista con las iniciales de los nombres 'lista_nombres'
2) Genere una lista con los nombres en minúscula de 'lista_nombres'
3) Genere una lista con los nombres/apellidos que empiezan por 'P'
'''
#Eduardo torres
primera_letra=[n[0] for n in lista_nombres]
print(primera_letra)

#Luis Alejandro
#Punto #2
minuscula_nombre = [x.lower() for x in lista_nombres]
#Punto #3
inicia_por_p = [x for x in lista_nombres if x[0].lower()=="p"]
print(inicia_por_p)